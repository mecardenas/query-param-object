package com.telefonica.parameter.pojo;

import lombok.Data;

@Data
public class Payment {

	private String id;
	
	private String type;
}
