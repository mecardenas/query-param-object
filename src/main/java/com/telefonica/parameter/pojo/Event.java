package com.telefonica.parameter.pojo;

import lombok.Data;

@Data
public class Event {

	private String subscriber;

	private CreateTopUpRequest createTopUpRequest;
}
