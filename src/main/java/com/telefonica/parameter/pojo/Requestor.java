package com.telefonica.parameter.pojo;

import lombok.Data;

@Data
public class Requestor {
	
	private String id;
}
