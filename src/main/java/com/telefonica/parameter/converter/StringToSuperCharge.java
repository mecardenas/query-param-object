package com.telefonica.parameter.converter;

import java.io.IOException;
import java.util.List;

import org.springframework.core.convert.converter.Converter;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.telefonica.parameter.pojo.SuperCharge;

public class StringToSuperCharge implements Converter<String, List<SuperCharge>>{

	@Override
	public List<SuperCharge> convert(String source) {
		try {
			return new ObjectMapper().readValue(source, new TypeReference<List<SuperCharge>>() {});
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}
	
}
