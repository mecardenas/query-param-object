package com.telefonica.parameter.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.telefonica.parameter.pojo.SuperCharge;

import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
public class ProductOrderController {

	@GetMapping(value = "/object-parameter")
	public ResponseEntity<List<SuperCharge>> getObjectParameter(
			@RequestParam(name = "objects", required = false) List<SuperCharge> superCharges,
			@RequestParam(name = "additional", required = false) String additional)
	{
		log.info("[RequestParam] objects : {}", superCharges);
		log.info("[RequestParam] additional : {}", additional);
		return new ResponseEntity<>(superCharges, HttpStatus.OK);
	}

}
